# 2D game

## Development platform

I'm doing the development on Ubuntu 12.04 64-bit. Therefore may the compile
script and the code need tweaking to be runnable on other platforms. I will
work more on this later on.

## Compile and run

To compile and run the game, execute the following command in the root folder:

    ./compile

## Documentation

* [SFML 2.0 API](http://sfml-dev.org/documentation/2.0/index.php)
* [SFML 2.0 Tutorials](http://www.sfml-dev.org/tutorials/2.0/)
* [Box 2D](http://box2d.org/documentation/)
