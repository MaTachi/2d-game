#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <iostream>

int main()
{
  sf::RenderWindow window(sf::VideoMode(200, 200), "SFML works!");
  sf::CircleShape shape(100.f);
  shape.setFillColor(sf::Color::Green);

  // Declare and load a texture
  sf::Texture texture;
  texture.loadFromFile("res/logo.png");

  // Create a sprite
  sf::Sprite sprite;
  sprite.setTexture(texture);
  sprite.setTextureRect(sf::IntRect(10, 10, 50, 30));
  sprite.setColor(sf::Color(255, 255, 255, 200));
  sprite.setPosition(100, 25);

  sf::Clock clock;
  //Time time = clock.getElapsedTime();

  float nextTime = 0;
  int counter = 0;

  while (window.isOpen())
  {
    sf::Event event;
    while (window.pollEvent(event))
    {
      if (event.type == sf::Event::Closed)
        window.close();
    }

    float time = clock.getElapsedTime().asSeconds();
    if (time > nextTime) {
      nextTime += 1;
      std::cout << counter << std::endl;
      counter = 0;
    }
    counter++;
    //std::cout << "TIME: " << clock.getElapsedTime().asSeconds() << std::endl;
    //std::cout << "TIME: ";

    window.clear();
    //window.draw(shape);
    // Draw it
    window.draw(sprite);
    window.display();
  }

  return 0;
}
